const WebpackBasedConfig = require('./webpack.config')
const { merge } = require('webpack-merge');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const pkg = require('./package.json');
const LIB_NAME = pkg.name;

module.exports = merge(WebpackBasedConfig, {
    mode: "development",
    devtool: 'eval-source-map',
    output: {
        filename: `${LIB_NAME}.js`,
        globalObject: 'this'
    },
    plugins: [
        ...WebpackBasedConfig.plugins,
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        })
    ],
});