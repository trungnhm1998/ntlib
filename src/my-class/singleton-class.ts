export class SingletonClass {
    private static _instance: SingletonClass;
    public static get instance(): SingletonClass {
        if (!SingletonClass._instance) {
            SingletonClass._instance = new SingletonClass();
        }
        return SingletonClass._instance;
    }

    public singletonFoo(): String {
        return "SingletonClass::foo";
    }
}