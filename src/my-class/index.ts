export class MyClass {
    public foo(): string {
        return "MyClass::Foo";
    }
}
export * from "./singleton-class";