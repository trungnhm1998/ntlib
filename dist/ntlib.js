/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["ntlib"] = factory();
	else
		root["ntlib"] = factory();
})(this, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\r\nvar __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });\r\n}) : (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    o[k2] = m[k];\r\n}));\r\nvar __exportStar = (this && this.__exportStar) || function(m, exports) {\r\n    for (var p in m) if (p !== \"default\" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);\r\n};\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\n__exportStar(__webpack_require__(/*! ./utils */ \"./src/utils.ts\"), exports);\r\n__exportStar(__webpack_require__(/*! ./my-class */ \"./src/my-class/index.ts\"), exports);\r\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9udGxpYi8uL3NyYy9pbmRleC50cz9mZmI0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBLDRFQUF3QjtBQUN4Qix3RkFBMkIiLCJmaWxlIjoiLi9zcmMvaW5kZXgudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tIFwiLi91dGlsc1wiO1xyXG5leHBvcnQgKiBmcm9tIFwiLi9teS1jbGFzc1wiOyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/index.ts\n");

/***/ }),

/***/ "./src/my-class/index.ts":
/*!*******************************!*\
  !*** ./src/my-class/index.ts ***!
  \*******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

eval("\r\nvar __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });\r\n}) : (function(o, m, k, k2) {\r\n    if (k2 === undefined) k2 = k;\r\n    o[k2] = m[k];\r\n}));\r\nvar __exportStar = (this && this.__exportStar) || function(m, exports) {\r\n    for (var p in m) if (p !== \"default\" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);\r\n};\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.MyClass = void 0;\r\nvar MyClass = /** @class */ (function () {\r\n    function MyClass() {\r\n    }\r\n    MyClass.prototype.foo = function () {\r\n        return \"MyClass::Foo\";\r\n    };\r\n    return MyClass;\r\n}());\r\nexports.MyClass = MyClass;\r\n__exportStar(__webpack_require__(/*! ./singleton-class */ \"./src/my-class/singleton-class.ts\"), exports);\r\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9udGxpYi8uL3NyYy9teS1jbGFzcy9pbmRleC50cz84OWE0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQTtJQUFBO0lBSUEsQ0FBQztJQUhVLHFCQUFHLEdBQVY7UUFDSSxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBQ0wsY0FBQztBQUFELENBQUM7QUFKWSwwQkFBTztBQUtwQix5R0FBa0MiLCJmaWxlIjoiLi9zcmMvbXktY2xhc3MvaW5kZXgudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgTXlDbGFzcyB7XHJcbiAgICBwdWJsaWMgZm9vKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIFwiTXlDbGFzczo6Rm9vXCI7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0ICogZnJvbSBcIi4vc2luZ2xldG9uLWNsYXNzXCI7Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/my-class/index.ts\n");

/***/ }),

/***/ "./src/my-class/singleton-class.ts":
/*!*****************************************!*\
  !*** ./src/my-class/singleton-class.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.SingletonClass = void 0;\r\nvar SingletonClass = /** @class */ (function () {\r\n    function SingletonClass() {\r\n    }\r\n    Object.defineProperty(SingletonClass, \"instance\", {\r\n        get: function () {\r\n            if (!SingletonClass._instance) {\r\n                SingletonClass._instance = new SingletonClass();\r\n            }\r\n            return SingletonClass._instance;\r\n        },\r\n        enumerable: false,\r\n        configurable: true\r\n    });\r\n    SingletonClass.prototype.singletonFoo = function () {\r\n        return \"SingletonClass::foo\";\r\n    };\r\n    return SingletonClass;\r\n}());\r\nexports.SingletonClass = SingletonClass;\r\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9udGxpYi8uL3NyYy9teS1jbGFzcy9zaW5nbGV0b24tY2xhc3MudHM/MDRmNSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQTtJQUFBO0lBWUEsQ0FBQztJQVZHLHNCQUFrQiwwQkFBUTthQUExQjtZQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFO2dCQUMzQixjQUFjLENBQUMsU0FBUyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7YUFDbkQ7WUFDRCxPQUFPLGNBQWMsQ0FBQyxTQUFTLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFTSxxQ0FBWSxHQUFuQjtRQUNJLE9BQU8scUJBQXFCLENBQUM7SUFDakMsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQztBQVpZLHdDQUFjIiwiZmlsZSI6Ii4vc3JjL215LWNsYXNzL3NpbmdsZXRvbi1jbGFzcy50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBTaW5nbGV0b25DbGFzcyB7XHJcbiAgICBwcml2YXRlIHN0YXRpYyBfaW5zdGFuY2U6IFNpbmdsZXRvbkNsYXNzO1xyXG4gICAgcHVibGljIHN0YXRpYyBnZXQgaW5zdGFuY2UoKTogU2luZ2xldG9uQ2xhc3Mge1xyXG4gICAgICAgIGlmICghU2luZ2xldG9uQ2xhc3MuX2luc3RhbmNlKSB7XHJcbiAgICAgICAgICAgIFNpbmdsZXRvbkNsYXNzLl9pbnN0YW5jZSA9IG5ldyBTaW5nbGV0b25DbGFzcygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gU2luZ2xldG9uQ2xhc3MuX2luc3RhbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaW5nbGV0b25Gb28oKTogU3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gXCJTaW5nbGV0b25DbGFzczo6Zm9vXCI7XHJcbiAgICB9XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/my-class/singleton-class.ts\n");

/***/ }),

/***/ "./src/utils.ts":
/*!**********************!*\
  !*** ./src/utils.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, exports) => {

eval("\r\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\r\nexports.foo = void 0;\r\nfunction foo() {\r\n    return \"hello world\";\r\n}\r\nexports.foo = foo;\r\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9udGxpYi8uL3NyYy91dGlscy50cz83ZGRhIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLFNBQWdCLEdBQUc7SUFDZixPQUFPLGFBQWEsQ0FBQztBQUN6QixDQUFDO0FBRkQsa0JBRUMiLCJmaWxlIjoiLi9zcmMvdXRpbHMudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gZm9vKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gXCJoZWxsbyB3b3JsZFwiO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/utils.ts\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.ts");
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});