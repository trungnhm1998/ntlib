const path = require('path');
const pkg = require('./package.json');
const LIB_NAME = pkg.name;

module.exports = {
    entry: './src/index.ts',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js', ''],
    },
    output: {
        library: LIB_NAME,
        libraryTarget: 'umd',
        filename: `${LIB_NAME}.js`,
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        // new DtsBundlePlugin()
    ]
}

function DtsBundlePlugin() { }
DtsBundlePlugin.prototype.apply = function (compiler) {
    compiler.hooks.afterEmit.tap('webpack-dts-bundle', () => {
        let dts = require('dts-bundle');
        dts.bundle({
            name: LIB_NAME,
            main: '@types/index.d.ts',
            out: '../@types/cc-index.d.ts',
            removeSource: false,
            outputAsModuleFolder: false,
            externals: true,
            referenceExternals: true
        });
    });
};