const WebpackBasedConfig = require('./webpack.config')
const { merge } = require('webpack-merge');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const pkg = require('./package.json');
const LIB_NAME = pkg.name;

module.exports = merge(WebpackBasedConfig, {
    mode: "production",
    devtool: 'source-map',
    output: {
        filename: `${LIB_NAME}.min.js`,
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                parallel: true,
                terserOptions: {
                    sourceMap: true
                }
            })
        ],
    },
    plugins: [
        ...WebpackBasedConfig.plugins,
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
});