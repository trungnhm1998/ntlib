export declare class SingletonClass {
    private static _instance;
    static get instance(): SingletonClass;
    singletonFoo(): String;
}
